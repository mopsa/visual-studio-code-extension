import path = require('path');
import * as vscode from 'vscode';

var diagnostics = vscode.languages.createDiagnosticCollection('mopsa');

function groupAlarmsByFile(alarms : any[]) {
    let map = new Map();
    for(var alarm of alarms) {
        let file = alarm.range.start.file;
        if (!path.isAbsolute(file) && vscode.workspace.workspaceFolders) {
            file = vscode.Uri.joinPath(vscode.workspace.workspaceFolders[0].uri, file).path;
        }
        if (!map.has(file)) {
            map.set(file, [alarm]);
        } else {
            let old = map.get(file);
            old.push(alarm);
            map.set(file, old);
        }
    }
    return map;
}

function addAlarms(alarms : any) {
    let map = groupAlarmsByFile(alarms);
    for (let [file, alarms] of map.entries()) {
        let uri = vscode.Uri.file(file);
        let diags = [];
        if (diagnostics.has(uri)) {
            for(let diag of diagnostics.get(uri) as vscode.Diagnostic[]) {
                diags.push(diag);
            }
        }
        for (var alarm of alarms) {
            const startLine = alarm.range.start.line - 1;
            const startColumn = alarm.range.start.column;
            const endLine = alarm.range.end.line - 1;
            const endColumn = alarm.range.end.column;
            const severity = alarm.kind === "warning" ? vscode.DiagnosticSeverity.Warning : vscode.DiagnosticSeverity.Error;
            const alarmDiagnostic = new vscode.Diagnostic(new vscode.Range(startLine, startColumn, endLine, endColumn), alarm.title + ': ' + alarm.messages, severity);
            diags.push(alarmDiagnostic);
        }
        diagnostics.set(uri, diags);
    }
}

class EnvironmentItem extends vscode.TreeItem {
    children : EnvironmentItem[];
    constructor(label : string, env : any) {
        if (!env) {
            super(label, vscode.TreeItemCollapsibleState.None);
            this.children = [];
            return;
        }
        if (typeof env === "string") {
            super(`${label}: ${env}`, vscode.TreeItemCollapsibleState.None);
            this.children = [];
            return;
        }
        super(label, vscode.TreeItemCollapsibleState.Expanded);
        this.children = [];
        for(let x in env) {
            this.children.push(new EnvironmentItem(x, env[x]));
        }
    }
}

class LocationItem extends vscode.TreeItem {
    private file : string;
    private line : number;
    constructor(file : string, line : number) {
        super(`${path.basename(file)}:${line}`);
        this.file = file;
        this.line = line;
        this.command = {
            title: '',
            command: 'vscode.open',
            arguments: [vscode.Uri.file(file)]
        };
    }
}

class TelescopeTreeProvider implements vscode.TreeDataProvider<EnvironmentItem | LocationItem> {
    private loc : LocationItem | null = null;
    private env : EnvironmentItem | null = null;
    private file : string | null = null;
    private line : number | null = null;
    private _onDidChangeTreeData: vscode.EventEmitter<EnvironmentItem | undefined | null | void> = new vscode.EventEmitter<EnvironmentItem | undefined | null | void>();
    readonly onDidChangeTreeData: vscode.Event<EnvironmentItem | undefined | null | void> = this._onDidChangeTreeData.event;
    
    constructor() {}

    getTreeItem(element: EnvironmentItem | LocationItem): vscode.TreeItem {
        return element;
    }

    getChildren(element?: EnvironmentItem | LocationItem): Thenable<(EnvironmentItem | LocationItem)[]> {
        if (element) {
            if (element instanceof EnvironmentItem) {
                return Promise.resolve(element.children);
            }
            return Promise.resolve([]);
        }
        if (!this.loc) {
            return Promise.resolve([]);    
        }
        let children : (EnvironmentItem | LocationItem)[] = [];
        children.push(this.loc);
        if (this.env) {
            children.push(...this.env.children);
        }
        return Promise.resolve(children);
    }

    setLocation(file : string, line : number) {
        this.loc = new LocationItem(file, line);
        this.env = null;
        this._onDidChangeTreeData.fire();
    }

    setEnvironment(env: any) {
        this.env = new EnvironmentItem('', env);
        this._onDidChangeTreeData.fire();
    }

    clear() {
        this.loc = null;
        this.env = null;
        this._onDidChangeTreeData.fire();
    }
}

function inspectEnvironment(telescopeProvider : TelescopeTreeProvider) {
    if (!vscode.window.activeTextEditor || !vscode.debug.activeDebugSession) {
        return;
    }
    let file = vscode.window.activeTextEditor.document.fileName;
    let line = vscode.window.activeTextEditor.selection.start.line + 1;
    telescopeProvider.setLocation(file, line);
    let loc = {file: file, line: line};
    vscode.debug.activeDebugSession.customRequest('environment', loc);
}


// Called when extention is activated
export function activate(context: vscode.ExtensionContext) {
	context.subscriptions.push(vscode.debug.registerDebugAdapterDescriptorFactory('mopsa', {
        createDebugAdapterDescriptor(_session: vscode.DebugSession, executable: vscode.DebugAdapterExecutable | undefined): vscode.ProviderResult<vscode.DebugAdapterDescriptor> {
            diagnostics.clear();
            var analyzer = _session.configuration.analyzer;
            var options = _session.configuration.options;
            options.push("-engine=dap");
            return new vscode.DebugAdapterExecutable(analyzer, options);
        }
    }));
    
    // Shows the messages between vscode and mopsa (dap) 
    vscode.debug.registerDebugAdapterTrackerFactory('*', {
        createDebugAdapterTracker(session: vscode.DebugSession) {
            return {
                onDidSendMessage: m => {
                    if (m.type === "event" &&
                        m.event === "output" &&
                        m.body.category === "important" &&
                        m.body.data.kind === "alarms")
                    {
                        addAlarms(m.body.data.alarms);
                    }
                    if (m.type === 'response' &&
                        m.command === 'environment')
                    {
                        telescopeProvider.setEnvironment(m.body);
                    }
                },
                onWillStopSession() {
                    telescopeProvider.clear();
                },
                onExit() {
                    telescopeProvider.clear();
                }
            };
        }
    });

    let telescopeProvider = new TelescopeTreeProvider();

    vscode.window.createTreeView('telescope', {
        treeDataProvider: telescopeProvider 
    });

    context.subscriptions.push(vscode.commands.registerCommand('mopsa.inspect', () => inspectEnvironment(telescopeProvider)));

}

export function deactivate() {}
